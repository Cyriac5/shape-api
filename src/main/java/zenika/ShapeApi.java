package zenika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShapeApi {
    public static void main(String[] args){
        SpringApplication.run(ShapeApi.class, args);
    }
}
